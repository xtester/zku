export const isNull = value => value == null

export function extend() {
/*
	* Successfuly snatched and renamed from https://stackoverflow.com/a/28741811
*/
	var result = {}, obj;

	for (var i = 0; i < arguments.length; i++) {
		obj = arguments[i];
		for (var key in obj) {
			if (Object.prototype.toString.call(obj[key]) === '[object Object]') {
				if (typeof result[key] === 'undefined') {
					result[key] = {};
				}
				result[key] = extend(result[key], obj[key]);
			} 
			else {
				result[key] = obj[key];
			}
		}
	}
	return result;
}