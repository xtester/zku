import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import config from '@/config'

import {isNull, extend} from '@/common'

Vue.use(Vuex)

const defaultState = () => {
	return {
		completed: false,
		date: {
			current: null,
			prev: null,
			next: null
		},

		items: {

			water: {
				hot: {
					prev: null,
					price: null,
					current: null,
					count: null,
					cost: null,
					error: false,
				},

				cold: {
					prev: null,
					price: null,
					current: null,
					count: null,
					cost: null,
					error: false,

				},

				drain: {
					price: null,
				},
			},

			power: {
				t1: {
					prev: null,
					current: null,
					price: null,
					count: null,
					cost: null,
					error: false,
				},

				t2: {
					prev: null,
					current: null,
					price: null,
					count: null,
					cost: null,
					error: false,
				},

				t3: {
					prev: null,
					current: null,
					price: null,
					count: null,
					cost: null,
					error: false,
				}
			}
		}
	}
}

export default new Vuex.Store({
	state: defaultState(),

	getters: {
		water: state => state.items.water,
		power: state => state.items.power,
		waterDrainCount: state => !isNull(state.items.water.cold.count) && !isNull(state.items.water.hot.count) ? state.items.water.cold.count + state.items.water.hot.count : null,
		waterDrainCost: (state,getters) => !isNull(getters.waterDrainCount) ? getters.waterDrainCount * state.items.water.drain.price : null,
		waterCost: (state,getters) => !isNull(state.items.water.cold.cost) && !isNull(state.items.water.hot.cost) && !isNull(getters.waterDrainCost) ? state.items.water.cold.cost + state.items.water.hot.cost + getters.waterDrainCost : null,

		powerCost: (state,getters) => !isNull(state.items.power.t1.cost) && !isNull(state.items.power.t2.cost) && !isNull(state.items.power.t3.cost) ? state.items.power.t1.cost + state.items.power.t2.cost + state.items.power.t3.cost : null,
		// powerCost: (state,getters) => 77777,
		overallCost: (state,getters) => !isNull(getters.waterCost) && !isNull(getters.powerCost) ? getters.waterCost + getters.powerCost : null,
		canSave: (state) => {
			return Object.getOwnPropertyNames(state.items).every(item => {
				return Object.getOwnPropertyNames(state.items[item]).every(type => {
					return !state.items[item][type].hasOwnProperty('current') || (!isNull(state.items[item][type].current) && !state.items[item][type].error)
				})
			})
		}
	},


	mutations: {
		

		setItems(state, payload) {
			state.items = extend(state.items, payload)
		},

		setItem(state, {item,type,value}) {
			if (item && type) {
				const currItemType = state.items[item][type]
				if (!currItemType.hasOwnProperty('current')) {
					return
				}

				currItemType.current = !isNull(value) && !isNaN(parseInt(value)) ? parseInt(value) : null
			}
		},

		calcItem(state, {item,type}) {
			if (item && type) {
				const currItemType = state.items[item][type]
				if (!currItemType.hasOwnProperty('current')) {
					return
				}


				if (currItemType.prev && currItemType.current && currItemType.current >= currItemType.prev) {
					currItemType.count = currItemType.current - currItemType.prev
					currItemType.cost = currItemType.count * currItemType.price
				} else {
					currItemType.count = null
					currItemType.cost = null
				}
			}
		},

		setCompleted(state, completed) {
			state.completed = completed
		},


		setDate(state, payload) {
			state.date = payload
		},

		setItemError(state, {item, type, error}) {
			state.items[item][type].error = !!error
		},

		resetState(state) {
			Object.assign(state, defaultState())
		},

		resetErrors(state) {
			for (let item in state.items) {
				for (let type in state.items[item]) {
					state.items[item][type].error = false
				}
			}
		},

		resetCurrent(state) {
			for (let item in state.items) {
				for (let type in state.items[item]) {
					if (state.items[item][type].hasOwnProperty('current')) {
						state.items[item][type].current = null
					}
				}
			}
		}




	},

	actions: {
		calculateItem({commit,state}, {item,type,value}) {
			commit('setItem', {item, type, value})
			commit('calcItem', {item,type})
			
			commit('setItemError', {item, type, error: isNull(state.items[item][type].current) || (state.items[item][type].prev && isNull(state.items[item][type].count))})
		},

		recalculate({commit, state}) {

			for (let item in state.items) {
				for (let type in state.items[item]) {
					// console.log('Item: ', item, type)
					commit('calcItem', {item, type})
				}
			}
		},

		async loadData({commit,dispatch}, date) {
			// commit('resetState')
			

			const url = config.APP_URL + (date ? date : '')
			console.log(url)
			const response = await axios.get(url)
			const data = response.data
			if (response.data.success) {
				commit('resetErrors')
				commit('setItems', data.items)
				commit('setDate', data.date)
				commit('setCompleted', data.completed)


				dispatch('recalculate')
			}
		},

		async saveData({commit, state}, date) {
			commit('setCompleted', true);

			let data = {}
			for(let item in state.items) {
				data[item] = {}
				for (let type in state.items[item]) {
					if (state.items[item][type].hasOwnProperty('current')) {
						data[item][type] = state.items[item][type].current
					}
				}
			}

			console.log('Got data: ', {...data})
			try {
				const res = await axios.post(config.APP_URL + (date ? date : ''), JSON.stringify(data))
				console.log('axios res: ', res)
			} catch (e) {
				console.log('Got axios exception: ', e)
			}
			
			
		},

		changeData({commit,dispatch}) {
			commit('resetCurrent');
			dispatch('recalculate');
			commit('setCompleted', false);
		}
	},
	modules: {
	}
})
