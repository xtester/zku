import Vue from 'vue'
import App from './App.vue'
import store from './store'

import axios from 'axios'

import 'bootstrap/dist/css/bootstrap.css';
import "font-awesome/css/font-awesome.min.css"; 

import '@/styles/normalize.css'
import '@/styles/global.css'

// import '@/styles/main.sass'

Vue.config.productionTip = false
Vue.prototype.$axios = axios



new Vue({
	store,
	render: h => h(App)
}).$mount('#app')
